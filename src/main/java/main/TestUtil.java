package main;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class TestUtil {
    private static final List<String> plikiDoWypakowania = ImmutableList.of(
            "chromedriver.exe",
            "testlist.xml",
            "users.properties"
    );

    public static void wypakujResources() {
        plikiDoWypakowania.forEach(TestUtil::wypakujPlikZResource);
    }

    public static void wypakujPlikZResource(String name) {
        try {
            InputStream chromeExe = TestUtil.class.getClassLoader().getResourceAsStream(name);
            File doWypakowania = new File(name);
            if (!doWypakowania.exists()) {
                FileUtils.copyInputStreamToFile(chromeExe, doWypakowania);
            }
        } catch (Exception e) {
            throw new RuntimeException("Błąd podczas wypakowania: " + name, e);
        }
    }
}

