package main;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.collections.Lists;

import java.util.List;

public class Main {
    static List<String> suites = Lists.newArrayList();

    public static void main(String[] args) {
        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
        suites.add("testlist.xml");
        testng.setTestSuites(suites);
        testng.run();
    }
}
