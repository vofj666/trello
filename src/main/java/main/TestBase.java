package main;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    protected static String BASEURL = "https://trello.com/";
    protected static WebDriver webDriver;
    protected static Properties properties;
    protected static WebDriverWait wait;

    public TestBase() {
        try {
            properties = new Properties();
            InputStream ip = this.getClass().getClassLoader().getResourceAsStream( "users.properties" );
            properties.load(ip);
            TestUtil.wypakujResources(  );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @BeforeMethod
    public static void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920x1080");
        System.setProperty("webdriver.google.chrome", "chromedriver.exe");
        webDriver = new ChromeDriver(options);
        wait = new WebDriverWait(webDriver,6);

//        webDriver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        if (ITestResult.SKIP != result.getStatus()) {
            webDriver.quit();
        }
    }

    @AfterMethod
    public void screenOnFail(ITestResult result) {
        if (ITestResult.FAILURE == result.getStatus()) {
            try {
                captureScreenshot();
            } catch (Exception e) {
            }
        }
    }

    public String getCurrentDate() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }

    public void captureScreenshot() {
        try {
            TakesScreenshot ts = (TakesScreenshot) webDriver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File("test-output/screenshots/" + getClass().getName() + "_" + getCurrentDate() + ".png"));
            System.out.println("Wykonano zrzut ekranu");
        } catch (Exception e) {
            System.out.println("Wystąpił wyjątek: " + e.getMessage());
        }
    }

    public void retryingFindClick(By by) throws InterruptedException {
        int attempts = 0;
        boolean failed = false;
        Exception currentException = new Exception();
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(by)));
        while (attempts < 5) {
            try {
                webDriver.findElement(by).click();
                failed = false;
                break;
            } catch (Exception e) {
                currentException = e;
                failed = true;
            }
            attempts++;
            Thread.sleep(50);
        }
        if (failed) {
            Assert.fail("Nie wykonano kroku " + currentException.getMessage());
        }
        Thread.sleep(150);
    }

    public void sendKeys(By by, String text) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        webDriver.findElement(by).sendKeys(text);
        Thread.sleep(150);
    }

    public void clearAndSendKeys(By by, String text) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        webDriver.findElement(by).clear();
        Thread.sleep(150);
        sendKeys(by, text);
    }
}
