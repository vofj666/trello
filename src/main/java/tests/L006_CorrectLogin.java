package tests;

import main.TestBase;
import org.testng.annotations.Test;
import pages.BoardsPage;
import pages.LoginPage;

public class L006_CorrectLogin extends TestBase {
    private LoginPage loginPage = new LoginPage();
    private BoardsPage boardsPage = new BoardsPage();

    @Test
    //Logging in via email (correct data) -> Path by button at the top right of the page ‘Log in’
    public void test_L006() throws InterruptedException {
        webDriver.get(BASEURL);
        Thread.sleep(2000);
        loginPage.logIn(properties.getProperty("user1"),properties.getProperty("pass1"));

        boardsPage.assertion_CanWeLogOut();
    }
}
