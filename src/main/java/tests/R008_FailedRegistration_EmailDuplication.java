package tests;

import main.TestBase;
import org.testng.annotations.Test;
import pages.LoginPage;

public class R008_FailedRegistration_EmailDuplication extends TestBase {
    private LoginPage loginPage = new LoginPage();

    @Test
    //Registration via email (wrong email – already used) -> Path by button at the top right of the page ‘Sign Up’  / button ‘or create an account’ at top
    public void test_L006() throws InterruptedException {
        webDriver.get(BASEURL);
        Thread.sleep(2000);
        loginPage.register("g108g@mail-file.net","g","test12345");

        loginPage.assertion_ErrorComparsion("Email already in use by another account. You can use log in or use the forgot password page to reset your password.");
    }
}
