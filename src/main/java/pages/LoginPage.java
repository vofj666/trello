package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class LoginPage extends HomePage {
    public void logIn(String email, String password) throws InterruptedException {
        button_LogIn();
        sendKeys(By.id("user"),email);
        sendKeys(By.id("password"),password);
        retryingFindClick(By.id("login"));
        Thread.sleep(1500);
    }

    public void register(String email, String name, String password) throws InterruptedException {
        button_SignUp();
        sendKeys(By.id("email"),email);
        retryingFindClick(By.id("signup"));
        Thread.sleep(1000);
        sendKeys(By.id("name"),name);
        sendKeys(By.id("password"),password);
        retryingFindClick(By.id("signup"));
        Thread.sleep(1000);
    }

    public void assertion_ErrorComparsion(String errorText){
        List<WebElement> errors = webDriver.findElements(By.id("error"));
        if (errors.size() == 1){
            Assert.assertEquals(webDriver.findElement(By.id("error")).getText(),errorText);
        } else {
            Assert.fail("Didn't found proper error warning");
        }
    }
}
