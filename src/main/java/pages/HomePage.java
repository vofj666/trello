package pages;

import main.TestBase;
import org.openqa.selenium.By;

public class HomePage extends TestBase {

   public void button_LogIn() throws InterruptedException {
       retryingFindClick(By.cssSelector("a.btn-sm:nth-child(1)"));
   }

   public void button_SignUp() throws InterruptedException {
       retryingFindClick(By.cssSelector("a.btn-sm:nth-child(2)"));
   }
}
