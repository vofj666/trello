package pages;

import main.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class BoardsPage extends TestBase {

    public void expand_UserActions() throws InterruptedException {
        retryingFindClick(By.cssSelector("._24AWINHReYjNBf"));
    }

    public void assertion_CanWeLogOut() throws InterruptedException {
        expand_UserActions();

        List<WebElement> logoutButton = webDriver.findElements(By.cssSelector("._3n2uNSrVwAmo1u > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(8) > button:nth-child(1) > span:nth-child(1)"));
        Assert.assertTrue(logoutButton.size() == 1);
    }
}
